﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Un programa que pida al usuario 4 números, los memorice (utilizando una tabla),  

             calcule su media aritmética y después muestre en pantalla la media y los datos tecleados.*/



            Console.WriteLine("Introduce 4 numeros");

            int[] num = new int[5];

            num[0] = Convert.ToInt32(Console.ReadLine());

            num[1] = Convert.ToInt32(Console.ReadLine());

            num[2] = Convert.ToInt32(Console.ReadLine());

            num[3] = Convert.ToInt32(Console.ReadLine());

            num[4] = (num[0] + num[1] + num[2] + num[3]) / 4;

            Console.WriteLine("los numero ingresado fueron");

            Console.WriteLine("El primer numero es: " + num[0]);

            Console.WriteLine("El segundo numero es: " + num[1]);

            Console.WriteLine("El tercer numero es: " + num[2]);

            Console.WriteLine("El cuarto numero es: " + num[3]);

            Console.WriteLine("La media de los numero ingresado es: " + num[4]);

            Console.ReadKey();
        }
    }
}
