﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Un programa que pida al usuario 5 números reales (pista: necesitarás un array de "float") y  

            luego los muestre en el orden contrario al que se introdujeron.*/



            Console.WriteLine("Introduce 5 numeros reales");

            float[] num = new float[5];

            num[0] = Convert.ToSingle(Console.ReadLine());

            num[1] = Convert.ToSingle(Console.ReadLine());

            num[2] = Convert.ToSingle(Console.ReadLine());

            num[3] = Convert.ToSingle(Console.ReadLine());

            num[4] = Convert.ToSingle(Console.ReadLine());
            
            Console.Write("los numero ingresados fueron el " + num[4] + "," + num[3] + "," + num[2] + ","

                + num[1] + "," + num[0]);

            Console.ReadKey();
        }
    }
}
